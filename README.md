
<h2> Hi, I'm Samiullah! <img src="https://www.funimada.com/assets/images/cards/big/hello-11.gif" width="70" height="40"></h2>
<p><img src="https://img.shields.io/badge/Sami%20Ullah-is%20Available%20for%20Hire%20-blueviolet" alt=""></p>
<img align='right' src="https://i.pinimg.com/originals/9e/75/9f/9e759fd37ccd98da121b670249f34afa.gif" width="230">
<p><em>Remote Full-stack web developer ready for adventure</em><img src="https://media.giphy.com/media/XGma2iRIHTKkwqRkFl/giphy.gif" width="50"></p>

[![Linkedin Badge](https://img.shields.io/badge/-Samiullah-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/ellievillalejos/)](https://www.linkedin.com/in/samiullah-khan-2702b7171/)
[![Twitter Badge](https://img.shields.io/badge/-samiullah_-1ca0f1?style=flat-square&labelColor=1ca0f1&logo=twitter&logoColor=white&link=https://twitter.com/miss_elliev)](https://twitter.com/samiullahk997)
[![ThePracticalDev Badge](https://img.shields.io/badge/-samiullah-0A0A0A?style=flat-square&labelColor=black&logo=dev.to&link=https://dev.to/misselliev)](https://dev.to/misselliev)
<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://codepen.io/samiullahk997" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/codepen.svg" alt="@samiullahk997" height="30" width="40" /></a>
<a href="https://twitter.com/samiullahk997" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/twitter.svg" alt="@samiullahk997" height="30" width="40" /></a>
<a href="https://www.linkedin.com/in/sami-full-stack/" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="@samiullahk997" height="30" width="40" /></a>
<a href="https://www.instagram.com/samiullahk997/" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="@samiullahk997" height="30" width="40" /></a>
<a href="https://www.codechef.com/users/samiullahk997/" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.1.0/icons/codechef.svg" alt="@samiullahk997" height="30" width="40" /></a>
<a href="https://www.hackerrank.com/samiullahk997" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/hackerrank.svg" alt="@samiullahk997" height="30" width="40" /></a>
<a href="https://leetcode.com/samiullahk997/" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/leet-code.svg" alt="@samiullahk997" height="30" width="40" /></a>
<a href="https://platform.topcoder.com/profile/samiullahk997" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/topcoder.svg" alt="samiullahk997" height="30" width="40" /></a>
</p>
### <img src="https://media.giphy.com/media/kbVuid1Ak3uEHJUMVO/giphy.gif" width="50"> A little more about me...  

```javascript
const Samiullah = {
  lookingFor: "Front-end developer" || "Full-stack web developer",
  pronouns: ["He", "Him"],
  code: [JavaScript, React, Redux, Ruby on Rails, HTML/CSS, Semantic UI, Bootstrap, 
  Tailwind CSS, Material UI],
  askMeAbout: [Chihuahuas, The 1975, food recipes, renovation shows],
  challenge: "I am honing my React skills and picking up TypeScript",
  funFacts: ['I have 3+ years of remote work experience with devs 
  from all over the world', 
  'my tiny chihuahua dog is ready to woof at the right offer']
}
```
### This week I spent my time on<img src="https://media.giphy.com/media/SvQzkTQb3ZwKcj1QTO/giphy.gif" width="40">

<!--START_SECTION:waka-->

```text
JavaScript   11 hrs 14 mins  ██████████████████████░░░   87.58 %
SCSS         1 hr 35 mins    ███░░░░░░░░░░░░░░░░░░░░░░   12.42 %
```

<!--END_SECTION:waka-->

<p>Latest Blog Posts<img src="https://media.giphy.com/media/THICzXhqZItpoFX7aD/giphy.gif" width="55"></p>

<!-- BLOG-POST-LIST:START -->
- [Upgrade your React game with TypeScript: Routes](https://dev.to/misselliev/upgrade-your-react-game-with-typescript-routing-4c59)
- [Upgrade your React game with TypeScript: More on Types](https://dev.to/misselliev/upgrade-your-react-game-with-typescript-more-on-types-5o8)
- [Show off with Github&#39;s README](https://dev.to/misselliev/show-off-with-github-s-readme-40eh)
<!-- BLOG-POST-LIST:END -->

<p>You can check out my portfolio <a href="https://elizabeth-villalejos.netlify.app">here</a><img src="https://media.giphy.com/media/cKPse5DZaptID3YAMK/giphy.gif" width="60"></p>
